from django.db import models

# Create your models here.

class Kuliah(models.Model):
    mata_kuliah = models.CharField(max_length = 200)
    dosen = models.CharField(max_length = 200)
    jumlah_sks = models.IntegerField()
    deskripsi = models.TextField()
    semester = models.CharField(max_length = 200)
    ruang_kelas = models.CharField(max_length = 200)

