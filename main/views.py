from django.shortcuts import render,redirect
from .forms import KuliahForm
from .models import Kuliah


def home(request):

    if request.method == "POST":
        form = KuliahForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('main:home')
    else:
        form = KuliahForm
    
    context = {'form' : form}
    return render(request, 'main/home.html', context)

def display(request):

    isi = Kuliah.objects.all()
    context = {'isi' : isi}

    return render(request, 'main/display.html', context)

def del_matkul(request, mata_kuliah):

    isi = Kuliah.objects.filter(mata_kuliah = mata_kuliah)

    isi.delete()

    return redirect('main:home')

def detail(request, mata_kuliah):

    isi = Kuliah.objects.filter(mata_kuliah = mata_kuliah)

    context = {'isi' : isi} 

    return render(request, 'main/detail.html', context)