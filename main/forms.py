from django import forms
from .models import Kuliah
from django.forms import ModelForm

##Create your forms here

class KuliahForm(ModelForm):
    class Meta:
        model = Kuliah
        fields = '__all__'
        