from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('display', views.display, name='display'),
    path('del_matkul/<str:mata_kuliah>', views.del_matkul, name='del_matkul'),
    path('detail/<str:mata_kuliah>', views.detail, name='detail')
]
